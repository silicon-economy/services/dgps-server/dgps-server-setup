Base Station Guide
============

Base Station Setup
------------
This section describes how the computer of the Base Station has to be setup to create
a RTK server. This server sends the differential GPS correction data, also called RTCM data, in the NTRIP format. 
NTRIP stands for Networked Transport of RTCM via Internet Protocol. Further information about NTRIP_ can be found here 
In detail, the server can be setup from a remote computer or directly
on the Base Station computer. In this documentation we assume that a remote computer is used.

Hardware Requirements
.....................

As RTK server, any computer which uses a Debian_ based operating system such as Ubuntu_ can be used.  
All described steps have been tested for Ubuntu 20.04. Nonetheless, the selected computer requires a 
stable network connection and a USB_ port.

The used GPS chip and antenna are described in the chapter :ref:`Chip Configuration`.

Software Requirements
.....................
This git repository contains multiple Ansible_ playbooks for an automatic setup of a DGPS server.

An introduction to Ansible_ can be found in its documentation.

We created a simple cli tools using the python library click_ to facilitate the usage of the playbook. Hence,
no knowledge of Ansible_ is required in the setup process. In detail, all requirements are installed during the setup
of the server.

.. _`Server Guide Prerequired Software`:

Prerequired Software 
'''''''''''''''''''''''''''''''''''''''''''''''''

Although all requirements are installed by Ansible_, Ansible requires a running SSH server which has to be installed on the Base Station computer
manually by following these steps:

1. Update and upgrade the apt repositories.
::

  sudo apt update && sudo apt upgrade

2. Install and start the openSSH SSH server.
::

  sudo apt install openssh-server python3

.. note::
  The installation of openssh-server is only necessary, if the Base Station should be setup from a remote computer.

3. Add your public ssh key to the server by typing the following command in the computer:
::

  ssh-copy-id <User-On-Server>@<DGPS-Server-IP>

.. note::
  For this step, it is assumed, that a functioning ssh-agent is present on the remote computer.

4. Clone our repository on the remote computer. 
::

  git clone LINK_TO_REPO

5. Install Ansible_ on the remote computer by executing our handy installation script inside the root of our repository using the following command:
::

  ./install-ansible.sh


Deployment of the RTK-Server Software
'''''''''''''''''''''''''''''''''''''

To deploy the setup from a remote computer, a cli tool was developed to facilitate
the deployment process. The detailed usage is described in the steps below.

1. To make use of the cli tool the IP address and the prefix_path in the site.yml file, which is the installation folder on the server need to be adjusted as pictured below.
::

  # example site.yml
  ---
  - name: Setup DGPS Server
    hosts: all
    vars:
      prefix_path: /home/rtk-server-user/rtk-server # installation folder on server
      ntrip_server_ip: 192.168.192.55 # ip of the NTRIP server

    roles:
     - common
     - ntrip-server
     - configuration_setup

2. The inventory.yml needs to be adjusted as well as displayed below.
::

  # example inventory.ym
  all:
    hosts:  # the definition of multiple hosts is allowed here
      RTK-Server:
        ansible_user: rtk-server-user # user which is used by ansible on the server
        ansible_host: 192.168.192.55 # ip of the server
        ansible_python_interpreter: /usr/bin/python3 # the used python version - do not change this
        prefix_path: /home/rtk-server-user/rtk-server # installation folder on server

3. Now, our cli tool can be used in the root of our repository to setup the server in a fully automatic manner.
::

  ./run-playbook.py [HOST]

.. note::
  Ansible_ will ask you to enter the sudo password of the deployment machines by showing the prompt *BECOME password:*.

.. note::
  After executing the Ansible_ script, a reboot of the server is required.


RTK Server Configuration Modes
------------------------------
To configure the server simple to use cli tool has been developed which can be executed using the following command:
::

  ./change-mode.py [MODE] [HOST]

In the next few sections, a detailed description of all modes is presented. The host in the above displayed command
references to the hosts which had been defined in the :ref:`Deployment of the RTK-Server Software` section.

Configuration
.............

The configuration mode of the server stops the RTK-Server. Now no RTK data is sent in NTRIP format. 
In addition, the serial port, which is connected to the
Ublox chip which is used by the NTRIP server to evaluate the current RTK correction signal, is forwarded to the TCP port
6000 and is available to be used in a configuration process using the U-Center_ software. A detailed description of the
interaction using U-Center_ can be found in the chip configuration chapter.

The configuration mode can be activated by executing the following script in the root of our repository:
::

  ./change-mode.py --mode configuration [HOST]

Publishing
..........
The publishing mode starts the publishing of the RTK correction data using a NTRIP server.
When this mode is used, the configuration mode is deactivated.

The publishing mode can be activated by executing the following script in the root of our repository:
::

  ./change-mode.py --mode publishing [HOST]

Restart
.......
The restart mode restarts the publishing of the RTK correction signal and stops all related services on the server.
When this mode is used, the configuration mode is deactivated.

The restart mode can be activated by executing the following script in the root of our repository:
::

  ./change-mode.py --mode restart [HOST]

Stop
....
The stop mode stops the publishing of the RTK correction signal and stops all related services on the server.
When this mode is used, the configuration mode is deactivated.

The stop mode can be activated by executing the following script in the root of our repository:
::

  ./change-mode.py --mode stop [HOST]



.. _Ansible: https://docs.ansible.com/
.. _Debian: https://www.debian.org/
.. _USB: https://de.wikipedia.org/wiki/Universal_Serial_Bus
.. _Ubuntu: https://releases.ubuntu.com/20.04/
.. _click: https://click.palletsprojects.com/
.. _U-Center: https://www.u-blox.com/en/product/u-center
.. _NTRIP: https://en.wikipedia.org/wiki/Networked_Transport_of_RTCM_via_Internet_Protocol
