RTK Server and Client Documentation
===================================

This documentation includes a step by step manual on how a RTK server (base station) and receiver (rover) can
be setup and configured in a few easy and largely automized steps as pictured in the image below.
In detail, we describe the required software configuration and deployment for the base station and rover.
We explain how to setup the necessary Ublox ZED-F9P chips for the base station and the rover.
Moreover, we show the validation steps to ensure a stable RTK correction. 
After all configuration, deployment, and validation steps have been successfully completed, the base station sends the correction data (RTK data) in 
NTRIP format over the internet and the rover receives and computes this data with its current GNSS position and then outputs a corrected GNSS signal.
Alternatively, correction data can be sent from other base stations for the rover to receive and process. 
If a second rover is set up, then another antenna and Ublox ZED-F9P chip is needed. Another NMEA GNSS driver and NTRIP client have to be started, for example on the same Linux PC of the first rover.


.. image:: images/RTK_GNSS_Server_Client_Architecture.png
  :width: 600
  :alt: Client Server Architecture

This guide assumes that both a base station and a rover are being set up. 
Therefore, it is strongly recommended that you first get an overview of the required components. We assume that you want a low cost setup. Some suggested components, for the Linux PC or the GNSS antenna can be replaced if desired. It has to be self-validated that these work in this setup.

.. table::
   :align: left
   :widths: auto
   
   ======== ======================== ==============================================
   Quantity  Component                Description
   ======== ======================== ==============================================
   2         `Ublox ZED-F9P`_        DGNSS receiver
   2         `Raspberry Pi`_         Linux PC
   2         `ANN-MB-00`_            GNSS antenna
   1         `C232HD-DDHSP-0`_       FTDI Chip USB-A 2.0 High-Speed-to-UART cable
   2         `USB-A-to-USB-C`_       USB 2.0-to-USB-C 3.1 cable
   ======== ======================== ==============================================


Second, get an overview how these components are connected with each other.

.. image:: images/wiring_diagram_base_station.png
  :width: 200
  :alt: Wiring diagram of the base station


.. image:: images/wiring_diagram_rover.png
  :width: 200
  :alt: Wiring diagram of the rover



After purchasing the hardware components and understanding their connections to each other, the required software is configured and set up. It is strongly recommended that you first complete all the steps in the Base Station Guide to 
ensure that correction data is sent. After that, the rover should be configured to receive the correction data, the configuration steps required for this are described in the Rover Guide. The validation steps include whether the RTK LED on the Ublox chip is flashing and the status and covariance matrix are correct, which can also be found in the Rover Guide.
A detailed documentation on the setup process of the base station and rover and their Ublox chips can be found on the pages, linked below. 


.. toctree::
  server_guide
  client_guide
  chip_configuration

Note that the terms GNSS and GPS are used interchangeably in this documentation.
The terms in the table below are going to be used through the documentation.

+----------------+---------------------------------------+
| Term           | Description                           |
+================+=======================================+
| client computer| The computer which deploys the Base   |
| or client      | station                               |
|                |                                       |
+----------------+---------------------------------------+
| RTK server or  | The computer which sends the          |
| base station   | correction data to the rover          |
|                |                                       |
+----------------+---------------------------------------+
| rover or       | The receiver of the correction data,  |
| receiver       | e.g. the computer in a robot          |
+----------------+---------------------------------------+

For the Fraunhofer IML base station slightly different components were used for the construction than suggested for this tutorial as shown in the following first two pictures from the left. The 3G+C defense antenna from navXperience is used, as well as an IPC instead of the suggested antenna and Linux PCs in the article list. 

For the Fraunhofer IML rover two Ublox ZED-F9P chips and ANN-MB-00 antennas are used to be able to determine the orientation of the robot. The NMEA GNSS drivers and NTRIP clients run on the same Raspberry Pi. 

Furthermore, the hardware components have been clad and fixed to withstand the weather and other external impacts.

.. image:: images/iml_base_station.png
  :width: 200
  :height: 350
  :alt: Interior of the base station

.. image:: images/iml_base_station_1.png
  :width: 200
  :height: 350
  :alt: Exterior of the base station

.. image:: images/iml_rover.png
  :width: 250
  :height: 350
  :alt: Interior of the rover


.. _`Ublox ZED-F9P`: https://www.sparkfun.com/products/15136
.. _`Raspberry Pi`: https://de.rs-online.com/web/p/raspberry-pi/1373331
.. _`ANN-MB-00`: https://www.marotronics.de/u-blox-GNSS-Multiband-Antenne-ANN-MB-00-IP67-SMA
.. _`C232HD-DDHSP-0`: https://de.rs-online.com/web/p/entwicklungstool-zubehor/7511172?cm_mmc=DE-PLA-DS3A-_-google-_-PLA_DE_DE_Raspberry+Pi+%26+Arduino+und+Entwicklungstools-_-(DE:Whoop!)+Entwicklungstool+Zubeh%C3%B6r-_-7511172&matchtype=&aud-813230962291:pla-339518476762&gclid=EAIaIQobChMI3uWRlsvh9gIVboODBx11Iwy3EAQYASABEgJLlPD_BwE&gclsrc=aw.ds
.. _`USB-A-to-USB-C`: https://de.rs-online.com/web/p/usb-kabel/1863056


