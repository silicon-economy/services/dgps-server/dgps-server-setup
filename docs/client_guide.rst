.. _`Receiver Guide`:

Rover Guide
==============

Client Setup
------------

This page describes how the Real Time Kinematic (RTK) signal receiver, that is running on the rover, must be configured to receive RTK correction data 
and Global Navigation Satellite System (GNSS) positon data.

Hardware Requirements
.....................

Any computer that uses Ubuntu 20.04, can be used as a RTK signal receiver, if it has two USB ports and a stable network connection.
The *Ublox ZED-F9P* chip with pinout, GNSS antenna and cables used, as well as their connection to the rover computer and the chip, are described on the page :ref:`Chip Configuration`\.

Software Requirements
..................... 
It is recommended to continue with the following steps when the *Ublox ZED-F9P* chip is configured according to the steps described in :ref:`Chip Configuration`\. After that, it is advisable to install_ and to `set up`_ ROS_ noetic on your computer if quick results are preferred, which is given by properly customizing and running launch files of the next steps.

Configuration and Deployment of the software modules
----------------------------------------------------

The software modules of the RTK signal receiver can be executed with or without ROS_, both options are described in this section. 

The first module is the *Networked Transport of RTCM via Internet Protocol* (NTRIP) client, which receives differential correction data 
from the NTRIP server of the base station. It is possible to receive the correction data from the self-deployed base station, or from other base stations. The self-deployed base station is further described on page the :ref:`Base Station Guide`. Base stations are available, e.g., on SAPOS_ or `RTK2go`_. 
The communication between the RTK server and the RTK signal receiver, is carried out over the Internet using the NTRIP protocol. 
NTRIP is a standard of the Radio Technical Commission for Maritime Services (RTCM), which is designed to distribute GNSS data in real-time through HTTP.

The second module is the National Marine Electronics Association (NMEA) GPS Driver, which parses GNSS Position data provided by the *Ublox ZED-F9P* chip. 
NMEA specifies the communication between GNSS/GPS receivers and many other electronic instruments. The terms GNSS and GPS are used synonymously here and in the following.


NTRIP Client
............

In order to allow the rover to receive the correction messages provided by the base station, the NTRIP client must be started first. 
Make sure that the USB to UART cable is connected to the computer and the chip.

ROS based
^^^^^^^^^
1. Please check out the repository `dgps_ros`_ into your ROS_ workspace by typing the following line in your terminal within the ``src`` folder of your workspace:
::

  git clone https://github.com/qinyunchuan/dgps_ros

2. Type the following command within ``dgps_ros`` folder to apply launch file and code adaptions to extend the parametrization possibilities of the including ros node:
::
    git apply <path-to-patches>/dgps_ros_launch_and_node_adaptions.patch

.. note::
    The required patch can be downloaded from the dgps_ros_patch_ repository


3. Build and source the `dgps_ros`_ package by typing, e.g., the following line in your terminal while in your ROS workspace root:
::

  catkin build && source devel/setup.bash


4. Adapt the following ``ntrip_client_ros_bringup`` launch file inside the ``dgps_ros/launch`` directory and adjust the values considering the server access data. The following credentials were used to configure the NTRIP server described on page the :ref:`Base Station Guide`. Alternatively, the access data of a base station can be requested and used, e.g., from the SAPOS_ or RTK2go_ website.
::
    <?xml version="1.0" encoding="UTF-8"?>
    <launch>
        <arg name="server" default="192.168.192.55"/>
        <arg name="port" default="2101"/>
        <arg name="username" default="dgps"/>
        <arg name="password" default="dgps"/>
        <arg name="mountpoint" default="STALL"/>
        <arg name="nmea" default=""/>
        <arg name="serialPort" default="/dev/ttyUSB0"/>
        <include file="$(find dgps_ros)/launch/dgps.launch">
            <arg name="server" value="$(arg server)" />
            <arg name="port" value="$(arg port)" />
            <arg name="username"  value="$(arg username)"/>
            <arg name="password"  value="$(arg password)"/>
            <arg name="mountpoint" value="$(arg mountpoint)" />
            <arg name="nmea" value = "$(arg nmea)"/>
            <arg name="serialPort" value="$(arg serialPort)" />
        </include>
    </launch> 

5. Execute the adapted launch file. Please note that the transmission of the access data is not encrypted.
::
    roslaunch dgps_ros ntrip_client_ros_bringup.launch

6. Check if correction data arrives, for example by reading from the USB port ``/dev/ttyUSB0``, where the *Ublox ZED-F9P* chip might be connected to the computer and the correction data is written, but it must be ensured that the rights are available for this.
::
    sudo cat < /dev/ttyUSB0



General purpose based
^^^^^^^^^^^^^^^^^^^^^

Please execute the following commands:

1. Check out this `NTRIP Client`_ repository by typing, e.g., the following:
::

  git clone https://github.com/nunojpg/ntripclient

2. Build the NTRIP client code from within the root of the cloned `NTRIP Client`_ repository.
::

    make    

3. Adapt the following command to the server's counterparts and run the NTRIP client. A detailed description of the individual parameters is contained in the README file. Please take into account that the transmission of the access data is not encrypted.
::

./ntripclient -S 192.168.192.55 -r 2101 -m STALL -u dgps -p dgps -D /dev/ttyUSB0 -B 115200


4. Check that correction data arrives. For instance, by reading from your USB port ``/dev/ttyUSB0`` where the correction data is written. It is necessary to ensure in advance that the rights for this are available.
::

    sudo cat < /dev/ttyUSB0


NMEA GNSS Driver
...............

The next steps handle the reading of the GNSS data from the USB port of the computer using the physical connection between the Ublox chip and the computer.
The receiving antenna is thereby connected to the Ublox chip. Make sure that the USB to USB-C cable connects the computer with the chip and the GNSS antenna is attached to the chip.

ROS based
^^^^^^^^^

1. Please check out the repository `nmea_serial_driver`_ into your ROS_ workspace by typing, e.g., the following command in your terminal while located in the ROS workspace ``src`` folder:
::

  git clone https://github.com/ros-drivers/nmea_navsat_driver.git

.. note::
    When using an other ROS version than noetic, checking out another branch than the master branch is maybe required.

2. Type the following command within ``nmea_serial_driver`` folder to apply the configuration of the launch file to the enclosing ros node:
::
    git apply <path-to-patches>/modified_nmea_serial_driver_launch_configuration.patch

.. note::
    The required patch can be downloaded at the nmea_navsat_driver_patch_ repository

3. Build and source the `nmea_serial_driver`_ package by typing, e.g., the following command in your terminal while in your ROS workspace root:
::

  catkin build && source devel/setup.bash

.. note::
    When using another shell environment than bash another file might has to be sourced - e.g. zsh is supported by source devel/setup.zsh.

4. Adapt the launch file for the `nmea_serial_driver`_ node if necessary. This can be done by creating a new file or adapting the file with the name ``nmea_serial_driver.launch`` inside the ``launch`` folder of the `nmea_serial_driver`_ package.
::

    <launch>
        <arg name="port" default="/dev/ttyACM0" />
        <arg name="baud" default="115200" />
        <arg name="frame_id" default="gps" />

        <arg name="use_GNSS_time" default="False" />
        <arg name="time_ref_source" default="gps" />
        <arg name="useRMC" default="False" />

        <group ns="ublox_single">
            <node name="nmea_serial_driver_node" pkg="nmea_navsat_driver" type="nmea_serial_driver" output="screen">
                <param name="port" value="$(arg port)" />
                <param name="baud" value="$(arg baud)" />
                <param name="frame_id" value="$(arg frame_id)" />
                <param name="use_GNSS_time" value="$(arg use_GNSS_time)" />
                <param name="time_ref_source" value="$(arg time_ref_source)" />
                <param name="useRMC" value="$(arg useRMC)" />
            </node>
        </group>
    </launch>

5. Check that the device ``/dev/ttyACM0`` exists, using the command shown below, else adapt the device name. Validate the values for the other parameters, read the documentation of the `nmea_serial_driver`_ package for support. The value of the parameter ``baud`` should correspond to the baud rate of the Ublox chip with 115200 as described in :ref:`Chip Configuration`\.
::

    ls /dev | grep ttyACM

6. Launch your created `nmea_serial_driver`_ launch file.
::
    roslaunch nmea_serial_driver nmea_serial_driver.launch

.. warning::
    The `nmea_serial_driver`_ will issue warnings because it cannot parse the UBX messages from the Ublox chip. Select ``Target -> USB`` in the ``Configuartion View`` of U-Center_ and check if Protocol in and out is set to NMEA. Alternatively the `ublox driver`_ can be used, which supports the UBX messages. 

7. Check that GPS data is published on the ``/ublox_single/fix`` topic of the `nmea_serial_driver`_ node by typing , e.g., the following command in a terminal where your current workspace has been sourced:
::

    rostopic echo /ublox_single/fix

.. note::
  To source a ros workspace the following command can be used, depending on your used shell environment - in our case bash has been used
  ::
    source <ROS_WORKSPACE_ROOT>/devel/setup.bash

.. note::
  The following steps have the requirement that the NTRIP client receives correction data and makes it available to the Ublox chip. In addition, the GNSS antennas of the rover and the base station should ideally be free of interference sources, and the placement of the antennas is best given in free space without obstacles. 

8. Check that the ``RTK`` LED is blinking as described in :ref:`LEDS of the Chip`\. 
::

9. Check that the status of the ``sensor_msgs/NavSatStatus`` message is set to ``2`` which indicates ground-based augmentation. If the chip does not receive correction data or cannot process it, the status is set to ``0``. This message is a part of the `sensor_msgs/NavSatFix`_ message published on the ``/ublox_single/fix`` topic.
::

10. Check that the values of the covariance matrix are small, this matrix is also a part of the `sensor_msgs/NavSatFix`_ message. A small covariance matrix indicates a small positioning error. 
::

If these validations work, the rover and base station have been set up correctly.

General purpose based
^^^^^^^^^^^^^^^^^^^^^

Another possibility is to write and run a python script or c++ program to read the GPS data from the USB port and process it without using ROS. If required, this has to be worked out independently.




.. _Debian: https://www.debian.org/
.. _Ubuntu: https://releases.ubuntu.com/20.04/
.. _`dgps_ros`: https://github.com/qinyunchuan/dgps_ros
.. _`nmea_serial_driver`: http://wiki.ros.org/nmea_navsat_driver
.. _install: http://wiki.ros.org/noetic/Installation/Ubuntu
.. _`set up`: https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_build.html
.. _`ublox driver`: https://github.com/KumarRobotics/ublox
.. _ROS: http://wiki.ros.org/noetic
.. _`NTRIP Client`: https://github.com/nunojpg/ntripclient
.. _U-Center: https://www.u-blox.com/en/product/u-center?lang=de
.. _SAPOS: https://sapos.de/
.. _`RTK2go`: http://rtk2go.com/
.. _`sensor_msgs/NavSatFix`: https://docs.ros.org/en/api/sensor_msgs/html/msg/NavSatFix.html
.. _dgps_ros_patch: https://gitlab.cc-asp.fraunhofer.de/dgps-server/dgps_ros_patch
.. _nmea_navsat_driver_patch: https://gitlab.cc-asp.fraunhofer.de/dgps-server/nmea_navsat_driver_patch
