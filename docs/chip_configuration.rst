
.. _`Chip Configuration`:

Ublox ZED-F9P Chip Configuration
================================


Remote Configuration Setup
--------------------------
The base station and rover have to be capable to interact properly with the *Ublox ZED-F9P* chipset. Both require unique configurations of the chip.

The remote configuration was inspired by this tutorial_.

Computer Setup
..............

To configure the *Ublox ZED-F9P* chip, make sure a USB to USB-C cable connects the *Ublox ZED-F9P* chip with the computer and the U-Center_ software is installed. 
This software has been tested on Windows and on Ubuntu by using Wine_.

After installing the U-Center_ software on the computer, the TCP connection to the *Ublox ZED-F9P* chip has to be added as shown in the below displayed pictures. 

|Configuring the TCP connection|
|Configuring the TCP connection2|

Configuration of the chip
-------------------------
Configuration files which can be used by the U-Center_ software can be found in the ``ublox_config`` folder of the ``DGPS-Server-Setup``. There is one configuration file for the base station and one for the rover. The specific config can be loaded by following those steps in the U-Center_ software:

1. Click ``View -> Generation 9 Configuration View``
2. Click ``Advanced Configuration``
3. Maximize the window
4. Click ``Load from file`` in the lower right corner
5. Select the needed configuration file in the ``ublox_config`` folder
6. Click ``View -> Configuration View``
7. Click ``Send`` to send the configuration to the *Ublox ZED-F9P* chip

.. warning::
  These steps had been tested for U-Center_ v21.02. Lower versions may differ!

Base Configuration
..................
Please follow the steps 1-7 listed above to load the enclosed base station config (F9P_Base.txt). We consider here that the base station outputs its correction data (RTCM messages) via the USB interface. Therefore an USB3 cable has to be connected to the USB interfaces between the chip and the computer.

.. note::
    Please don't forget so save the configuration changes that were made! If the configuration is done click ``View -> Configuration View`` and select ``CFG (Configuration)`` then check the Radiobox ``save current configuration`` and click ``send``. Otherwise the settings made are lost when the chip loses the power supply.

To check whether the base station chip has been configured correctly, please:

1. Click ``View -> Configuration View`` and select ``PRT (Ports)``. Make sure that for both incoming and outgoing protocol ``UBX+NMEA+RTCM3`` is selected.

|Port setup|

2. Select ``RATE (Rates)`` and check if the following is selected:

|Rate setup|

3. Select ``MSG (Messages)`` and check that for the following messages the USB checkbox is enabled:

- RTCM3.3 1005
- RTCM3.3 1074
- RTCM3.3 1084
- RTCM3.3 1094
- RTCM3.3 1124
- RTCM3.3 1230

To start the pulishing mode of the DGPS server ``./change-mode.py --mode pulishing`` needs to be executed as presented in :ref:`RTK Server Configuration Modes`.

.. _`Rover Configuration`:

Rover Configuration
...................
Please follow the steps 1-7 listed above to load the enclosed rover config (F9P_Rover.txt). We assume here that the rover receives its RTK data via the UART2 interface. See the picture below. The UART2 interface there is marked with "RTCM Correction". Furthermore, we assume that the rover outputs the corrected GNSS signal via the USB interface. Of course, it is also possible to receive the RTK data and output the corrected signal via other interfaces. This will not be described here.

.. note::
    Please don't forget so save the configuration changes that were made! If the configuration is done click ``View -> Configuration View`` and select ``CFG (Configuration)`` then check the Radiobox ``save current configuration`` and click ``send``. Otherwise the settings made are lost when the chip loses the power supply.

To check whether the rover has been configured correctly, please:

1. Click ``View -> Configuration View`` and select ``PRT (Ports)``.
In the following two interfaces of the rover chip have to be configured. The UART2 allows the rover to receive the RTCM data via the USB interface, the USB interface allows to output the corrected gps signal. Under UART2 RTCM3 must now be selected for both protocol in and protocol out. Please set the ``baud rate`` to ``115200``. In our setup we used the C232HD-DDHSP-0 `USB 2.0 to UART`_ cable to connect the UART2 interface to the rover computer. That is how the correction data is comming in.

.. warning::
    The C232HD-DDHSP-0 cable should not be confused with the C232HD-EDHSP-0 cable.

It is very important to use a 5V version NOT the 3V Version. Don't forget to switch Rx and Tx on the rover. The following image is a reference which shows how the *Ublox ZED-F9P* chip and the `USB 2.0 to UART`_ cable are connected. 

|chip_with_uart2|

|Port setup2|

2. Now we have to make sure that the corrected gps signal will be emitted by the USB interface. Please select ``Target -> USB`` and check if the following is clicked:

|Port setup3|

In our setup we selected a measurement period of 200 ms for the rover. Therefore:

3. Select ``RATE (Rates)`` and check if the following is selected:


|Rate setup|

4. Select ``MSG (Messages)`` and check that for the following messages the USB checkbox is enabled.

- RTCM3.3 1005
- RTCM3.3 1074
- RTCM3.3 1084
- RTCM3.3 1094
- RTCM3.3 1124
- RTCM3.3 1230

5. Start the NTRIP Client. Instructions for this can be found on the page :ref:`Receiver Guide`\.


.. _`LEDS of the Chip`:
LEDs of the Chip
----------------

The *Ublox ZED-F9P* chip distinguishes several LEDs as status indicators. The ``PWR`` LED will illuminate when the chip is powered over USB. The ``PPS`` LED for the second pulse lights up every second as soon as a position lock has been reached. The ``RTK`` LED lights up constantly when the device is switched on. Once RTCM data has been successfully received, it will start flashing. This is a good way to see if the *Ublox ZED-F9P* chip is receiving RTCM from various sources. Once an RTK fix is achieved, the LED will turn off. The ``FENCE`` LED can be used for geofencing applications.
|LEDs|





.. _tutorial: https://ozzmaker.com/using-u-center-to-connect-to-a-raspberry-pi-with-an-attached-berrygps-imu-gsm/
.. _U-Center: https://www.u-blox.com/en/product/u-center?lang=de
.. _Wine: https://wiki.ubuntuusers.de/Wine/
.. _`USB 2.0 to UART`: https://ftdichip.com/wp-content/uploads/2020/07/DS_C232HD_UART_CABLE.pdf


.. |Configuring the TCP connection| image:: images/ublox_remote.png 
    :alt: U-Blox Configuration
.. |Configuring the TCP connection2| image:: images/ublox_remote_2.png
    :alt: U-Blox Configuration
.. |LEDs| image:: images/LEDs.png
    :width: 400
    :alt: Status LEDs on the *Ublox ZED-F9P* chip
.. |Port setup| image:: images/base_ports.png 
    :alt: USB Port setup base station
.. |Rate setup| image:: images/RATE.png 
    :alt: Rate settings
.. |chip_with_uart2| image:: images/chip_with_uart2.png
    :alt: *Ublox ZED-F9P* chip connected with C232HD-DDHSP-0 cable
.. |Port setup2| image:: images/PRT1.png
    :alt: Port settings rover
.. |Port setup3| image:: images/PRT2.png 
    :alt: Port settings rover
