#!/usr/bin/env python3

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import click
import os


@click.command()
@click.argument('hosts', nargs=-1)
def run_playbook(hosts):
    cli = "ansible-playbook -K "

    # check if at least one host has been given
    if len(hosts) == 0:
        click.echo("FATAL ERROR: You need to specify at least one host for remote execution!", err=True)
        ctx = click.get_current_context()
        click.echo(ctx.get_help())
        ctx.exit()
        return
    cli += "-i inventory.yaml --limit {} site.yml".format(",".join(hosts))
    os.system(cli)


if __name__ == '__main__':
    run_playbook()
