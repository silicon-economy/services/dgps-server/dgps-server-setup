#!/bin/bash

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

set -e
set -o pipefail

ln -s ~/.ssh/id_rsa
ln -s ~/.ssh/id_rsa.pub
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt-get install -y ansible python3-click
echo Done.
