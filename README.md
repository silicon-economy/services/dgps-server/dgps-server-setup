> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

# DGPS Server

This repository contains all necessary components to setup the DGPS server and configuring the server.

The purpose of a DGPS Server is to provide position correction data in the RTCM format via NTRIP to clients which are using GPS antennas for the purpose
of outdoor localization of e.g. AGVs or UAVs.

The idea of the DGPS setup is shown in the image below.

![DGPS Setup]( docs/images/RTK_GNSS_Server_Client_Architecture.png "RTK server and client setup")

A detailed documentation is available in this [PDF](docs/DGPSServer.pdf).


