#!/usr/bin/env python3

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import click
import os


@click.command()
@click.option('--mode', default="stop", help="Mode of the DGPS Server\n"
                                             "publishing: configure the server to publish RTK data"
                                             " (start NTRIP server)\n\n"
                                             "configuration: configure the server to make the ZED-F9P chip"
                                             "configureable\n\n"
                                             "restart: restart NTRIP server\n\n"
                                             "stop: stops both, publishing and configuration mode (default)\n\n")
@click.argument('hosts', nargs=-1)
def change_mode(mode, hosts):
    cli = "ansible-playbook -K "

    # check if at least one host has been given
    if len(hosts) == 0:
        click.echo("FATAL ERROR: You need to specify at least one host for remote execution!", err=True)
        ctx = click.get_current_context()
        click.echo(ctx.get_help())
        ctx.exit()
        return

    cli += "-i inventory.yaml --limit {} ".format(",".join(hosts))

    # check which playbook has to be deployed
    if mode == "configuration":
        cli += "enable_remote_config.yml"
    elif mode == "publishing":
        cli += "enable_dgps_server.yml"
    elif mode == "restart":
        cli += "restart_ntrip.yml"
    elif mode == "stop":
        cli += "stop_everything.yml"
    else:
        click.echo("FATAL ERROR: You need to specify a mode!", err=True)
        ctx = click.get_current_context()
        click.echo(ctx.get_help())
        ctx.exit()
        return

    os.system(cli)


if __name__ == '__main__':
    change_mode()
